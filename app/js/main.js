var app = {
	init: function() {
		if( $( document ).width() < 576 ) {
			app.footer_toggle();
		}
		app.city_slider();
		app.thumb_slider();
		app.header_fixed();
		app.lang_popup();
		app.menu_open();
		app.menu_close();
		if( $( document ).width() > 767 ) {
			app.col_height_auto();
		}
	},
	footer_toggle: function() {
		$( '.menu-footer_js' ).each(function() {
			var btn = $(this).find( '.menu-footer__title_js' ),
				list = $(this).find('.menu-footer__list_js');
			btn.click(function(e) {
				e.preventDefault();
				list.slideToggle();
			})
		})
	},
	city_slider: function() {
		$( '.city-slider' ).owlCarousel({
			items: 3,
			loop: true,
			nav: false,
			dots: false,
			center: true,
			navText: ['<div class="city-slider__prev  chevron-left"></div>','<div class="city-slider__next  chevron-right"></div>'],
			autoplay: false,
			autoplayTimeout: 4000,
			autoplayHoverPause: true,
			smartSpeed: 500,
			autoplaySpeed: 1000,
			navSpeed: 500,
			responsive: {
				0: {
					items: 1,
					center: false
				},
				576: {
					items: 1,
					nav: true

				},
				992: {
					nav: true,
					center: true,
					items: 3
				},
				1200: {
					nav: true
				}
			},
		});
		$('.city-slider button').trigger('initialize.owl.carousel');
	},
	thumb_slider: function() {
		$('.thumb-for').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			fade: true,
			asNavFor: '.thumb-nav',
			responsive: [
				{
					breakpoint: 575,
					settings: {
						arrows: false
					}
				}
			]
		});
		$('.thumb-nav').slick({
			slidesToShow: 5,
			slidesToScroll: 1,
			asNavFor: '.thumb-for',
			dots: false,
			centerMode: true,
			focusOnSelect: true,
			responsive: [
				{
					breakpoint: 1199,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 575,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});

		var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var rv = -1;

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {

            if (isNaN(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))))) {
                if (navigator.appName == 'Netscape') {
                    var ua = navigator.userAgent;
                    var re = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
                    if (re.exec(ua) != null) {
                        rv = parseFloat(RegExp.$1);
                    }
                }
                else {
                }
            }
            else {
                if( parseInt( ua.substring(msie + 5, ua.indexOf(".", msie)) ) > 9 ) {
					$('.thumb-for').on('beforeChange', function(event, slick, currentSlide, nextSlide){
					  app.wow();
					});
                } 
            }
            return false;
        };
		

	},
	header_fixed: function() {
		$(window).resize(function() {
			$( '.under-header-js' ).css("padding-top", $(".header_fixed").outerHeight());
		}).resize();
	},
	lang_popup: function() {
		$( '.lang__box_popup_open' ).on('click', function(e) {
			e.preventDefault()
			$( '.lang__list' ).toggleClass('lang__list_visible');
			var link_in = $( '.lang__list' ).find('.lang__box');
			link_in.click(function() {
				$( '.lang__list' ).removeClass('lang__list_visible');
			});
			$(document).on('click', function (e) {
				if (!$(e.target).closest('.lang').length) {
					$( '.lang__list' ).removeClass('lang__list_visible');
				} 
			});
		})
	},

	menu_open: function() {
		$( '.hamburger_menu_open' ).click(function() {
			$( '.menu' ).addClass('menu_visible');
			$( 'body' ).addClass('no-scroll');
			$( 'header' ).toggleClass('header_margin_right');
		})
	},
	menu_close: function() {
		$( '.close_menu' ).click(function() {
			$( '.menu' ).removeClass('menu_visible');
			$( 'body' ).removeClass('no-scroll');
			$( 'header' ).removeClass('header_margin_right');
		})
	},
	wow: function() {
		 new WOW().init({
		 	mobile: false
		 });
	},

	col_height_auto: function() {
		$(window).resize(function() {
			$('.row').each(function() {
				var maxH = 0;
				var columns = $(this).find('.col-height-auto');

				columns.each(function() {
					$(this).css('min-height', 'auto');
					if ( $(this).height() > maxH ) {
						 maxH = $(this).outerHeight();
					}
				})
				columns.each(function() {
					$(this).css('min-height', maxH + 'px');
				})
			})
		}).resize();
	},
};
$(document).ready(function() {
	app.init();
})
$( window ).on('load', function() {
	app.wow();
})

