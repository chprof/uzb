'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    plumber = require('gulp-plumber'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    rimraf = require('rimraf'),
    concat = require("gulp-concat"),
    mainBowerFiles = require("main-bower-files"),
    browserSync = require("browser-sync"),
    reload = browserSync.reload;

var path = {
    build: {
        html: 'dist/',
        js: 'dist/js/',
        css: 'dist/css/',
        img: 'dist/images/',
        fonts: 'dist/fonts/'
    },
    src: {
        html: 'app/*.html',
        js: 'app/js/*.js',
        style: 'app/scss/main.scss',
        img: 'app/images/**/*.*',
        fonts: 'app/fonts/**/*.*'
    },
    watch: {
        html: 'app/**/*.html',
        js: 'app/js/**/*.js',
        style: 'app/scss/**/*.scss',
        img: 'app/images/**/*.*',
        fonts: 'app/fonts/**/*.*'
    },
    clean: './dist'
};

var config = {
    server: {
        baseDir: "./dist"
    },
    tunnel: true,
    host: 'localhost',
    port: 3000,
    logPrefix: "chprof"
};

gulp.task('webserver', function () {
    return browserSync(config);
});

gulp.task('clean', function (cb) {
    return rimraf(path.clean, cb);
});


gulp.task('html:build', function () {
    return gulp.src(path.src.html) 
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
    return gulp.src(path.src.js) 
        .pipe(rigger()) 
        .pipe(sourcemaps.init()) 
        // .pipe(uglify()) 
        .pipe(sourcemaps.write()) 
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
    return gulp.src(path.src.style) 
        .pipe(sourcemaps.init())
        .pipe(plumber())
        .pipe(sass({
            sourceMap: true,
            errLogToConsole: true
        }))
        .pipe(prefixer({
            browsers: ['last 20 versions'],
            cascade: false
        }))
        // .pipe(cssmin())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
    return gulp.src(path.src.img) 

        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});

gulp.task('awesome', function() {
    return gulp.src('bower/components-font-awesome/fonts/fontawesome-webfont.*')
        .pipe(gulp.dest('app/fonts/'));
});
gulp.task('fonts:build', function() {
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('build', [

    'html:build',
    'js:build',
    'style:build',
    'awesome',
    'fonts:build',
    'image:build'
]);


gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});


gulp.task('default', ['build', 'webserver', 'watch']);







gulp.task('bowerJs', function() {
    return gulp.src(mainBowerFiles('**/*.js'))
        // .pipe(concat('bundle.js'))
        .pipe(gulp.dest('app/js/libs/'))
})
gulp.task('bowerCss', function() {
    return gulp.src(mainBowerFiles('**/*.css'))
        .pipe(gulp.dest('app/scss/vendor/libs/'))
});

gulp.task('concatJs', function() {
    return gulp.src('app/js/libs/*.js')
        .pipe(concat('vendor.js'))
        // .pipe(uglify())
        .pipe(gulp.dest('app/js/'))
})
gulp.task('concatCss', function() {
    return gulp.src('app/scss/vendor/libs/*.css')
        .pipe(concat('_vendor.scss'))
        .pipe(gulp.dest('app/scss/vendor/'))
});


gulp.task('bower', ['bowerJs', 'bowerCss']);
gulp.task('concat', ['concatJs']);